<?php
/**
 * Created by PhpStorm.
 * User: Giannis
 * Date: 12/2/2017
 * Time: 5:44 μμ
 */

namespace Deamon;


class Signal
{

    private static $signal ='';

    public static function setSignal($signo)
    {
        self::$signal = $signo;
    }

    public static function getSignal()
    {
        return self::$signal;
    }


}