<?php
/**
 * Created by PhpStorm.
 * User: Giannis
 * Date: 11/2/2017
 * Time: 11:01 μμ
 */

namespace Deamon;


class FilePid
{

    private $pid = '';
    private $file = "file.pid";

    public function __construct($name)
    {
        $this->file = $name.".pid";
    }

    public function setPid($pid)
    {
        $this->pid = $pid;
        file_put_contents($this->file, $this->pid);
    }

    public function getPid()
    {
        return (int) file_get_contents($this->file);
    }

    public function deleteFile ()
    {
        if (file_exists($this->file)) {
            unlink($this->file);
        }
    }

    public static function createFile ($pid, $name)
    {
        $fpid = new FilePid($name);
        $fpid->setPid($pid);
    }

    public static function readFile ($name)
    {
        $fpid = new FilePid($name);
        return $fpid;
    }

}