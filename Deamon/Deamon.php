<?php
/**
 * Created by PhpStorm.
 * User: Giannis
 * Date: 11/2/2017
 * Time: 6:07 μμ
 */

namespace Deamon;
use Tools\Logger;
class Deamon
{
    const uid = 80;
    const gid = 80;


    public $file = '';
    private $class = '';

    public function registerProccess ($class)
    {
        $this->class = $class;

        posix_setuid(self::uid);
        posix_setgid(self::gid);

    }

    private function setUpSignal() {

        \pcntl_signal(SIGHUP,  function($signo) {
            Signal::setSignal($signo);
        });
    }


    public function run ()
    {

        $this->setUpSignal();

        for (;;) {
            Logger::Log("Running...");
            $this->class->run();
            Logger::Log("Stopped...");
        }

    }

    static function start ($class)
    {
        $pid = \pcntl_fork();
        if ($pid == -1) {
            die('could not fork');
        } else if ($pid) {
            // we are the parent
           // \pcntl_wait($status); //Protect against Zombie children
           Logger::Log($pid);
           exit($pid);
        } else {
           Logger::Log($pid);
           FilePid::createFile(getmypid(), get_class($class));

           $deamon = new Deamon();
           $deamon->registerProccess($class);
           $deamon->run();
        }

    }

    static public function stop ($name)
    {
        $pid = FilePid::readFile($name);
        posix_kill($pid->getPid(),9);
        $pid->deleteFile();
    }

    static public function reload ($name)
    {
        $pid = FilePid::readFile($name);
        posix_kill($pid->getPid(), SIGHUP);
    }

    static public function status($name)
    {
        $pid = FilePid::readFile($name);
        return system(sprintf("ps ax | grep %s | grep -v grep", $pid->getPid()));
    }


}