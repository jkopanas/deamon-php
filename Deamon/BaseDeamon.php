<?php
/**
 * Created by PhpStorm.
 * User: Giannis
 * Date: 12/2/2017
 * Time: 1:27 πμ
 */

namespace Deamon;
use Tools\Logger;

abstract class BaseDeamon
{

    public $pid = 0;

    public function run () {
        while (true) {

            Logger::Log("Next Loop");
            pcntl_signal_dispatch();
            $this->{$this->main}();
            if (Signal::getSignal() == SIGHUP) {
                Logger::Log("Signal Stop");
                break;
            }
            sleep(5);
        }
    }

}