<?php
/**
 * Created by PhpStorm.
 * User: Giannis
 * Date: 12/2/2017
 * Time: 7:21 μμ
 */

spl_autoload_register(function ($class_name) {
    include str_replace('\\', DIRECTORY_SEPARATOR, $class_name) . '.php';
});


use Deamon\Deamon;

if($argv[1] === 'stop'){
    Deamon::stop($argv[2]);
} else if($argv[1] === 'start') {
    Deamon::start(new Test());
} else if($argv[1] === 'reload') {
    Deamon::reload($argv[2]);
}