<?php
/**
 * Created by PhpStorm.
 * User: Giannis
 * Date: 11/2/2017
 * Time: 11:48 μμ
 */

use Deamon\BaseDeamon;

class Test extends BaseDeamon
{

    protected $main = 'main';
    private $file = 'numbers.log';
    private $fp = '';
    private $i = 0;

    public function __construct()
    {
        $this->fp = fopen($this->file, "w");
    }

    public function main()
    {
        fwrite($this->fp, $this->i);
        $this->i = $this->i+1;
    }


    public function __destruct()
    {
        fclose($this->fp);
    }


}