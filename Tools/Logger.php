<?php
/**
 * Created by PhpStorm.
 * User: Giannis
 * Date: 12/2/2017
 * Time: 8:19 μμ
 */

namespace Tools;


class Logger
{
    private $file = 'debug.log';
    private $fp = '';
    public function __construct()
    {
        $this->fp = fopen($this->file,"a+");
    }


    public static function Log($message)
    {
        $log = new Logger();
        $log->writeToLog($message);
    }

    public function writeToLog($message)
    {
        fwrite($this->fp,time()." Log Deamon: ".$message."\n");
        fclose($this->fp);
    }
}